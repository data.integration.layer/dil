#!/bin/bash

export CDD_SERVER_URL=https://cddirector.io
export CDD_TENANT_ID="96a95aae-12ff-423b-862a-63da57d194f5"
export CDD_API_KEY="eyJhbGciOiJIUzUxMiJ9.eyJ1c2VybmFtZSI6ImRhdGEuaW50ZWdyYXRpb24ubGF5ZXJAZ21haWwuY29tIiwidGVuYW50SWQiOiI5NmE5NWFhZS0xMmZmLTQyM2ItODYyYS02M2RhNTdkMTk0ZjUiLCJ1c2VySWQiOjEsImp0aSI6ImU2YWNlZTUwLTNhYmQtNDgyMy05ZWRjLTM0MGNlYmE1OWUxMiIsImV4cCI6MTYwNzYxMDk3OH0.E_Wg3pJpQ0ZwUD1X0MtcOfQWlP17ufNmJjo3HRXUbp9QYJrXq19isVcwoYlqeCI2HdKSGP12JcUSG1VbZ6qZ5Q"

export CI_PROJECT_NAME="dil"
export CI_BUILD_REF_NAME="master"
export CI_COMMIT_SHA="2ef78eefa7d45f5180434c7b4f39d7a2245689e0"

export DIL_REPOSITORY_NAME=$CI_PROJECT_NAME
export DIL_REPOSITORY_BRANCH=$CI_BUILD_REF_NAME
export DIL_REPOSITORY_BRANCH_GIT_COMMIT_ID=$CI_COMMIT_SHA
export DIL_REPOSITORY_BRANCH_BUILD_NUMBER=$DIL_REPOSITORY_BRANCH_GIT_COMMIT_ID

curl -s --header "Content-Type: application/json" --header "Accept: application/json" -d "{ \"applicationName\": \"$DIL_REPOSITORY_NAME\", \"applicationVersionBuildNumber\": \"$DIL_REPOSITORY_BRANCH_BUILD_NUMBER\", \"applicationVersionName\": \"$DIL_REPOSITORY_BRANCH\", \"commits\": [ { \"commitId\": \"$DIL_REPOSITORY_BRANCH_GIT_COMMIT_ID\" } ]}" "$CDD_SERVER_URL/cdd/design/$CDD_TENANT_ID/v1/applications/application-versions/application-version-builds" -H "Authorization: Bearer $CDD_API_KEY"
